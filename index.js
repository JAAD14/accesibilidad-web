window.onload = () => {
    document.querySelector(".arrow-right").addEventListener("click", clickRight);
    document.querySelector(".arrow-left").addEventListener("click", clickLeft);
    document
      .querySelector(".send-button")
      .addEventListener("click", showNotification);
  };
  
  /** Esta funcion se llama cuando la persona hace click en la fecha derecha del carousel para navegar a la derecha */
  function clickRight() {
    const currentLeft = parseInt(
      getComputedStyle(document.querySelector(".project-container")).left,
      10
    );
    if (currentLeft < -270) { //si el valor de izquierda es menor a -270, para de mover el contenido
      return;
    }
    let newValue = currentLeft - 270; //270 toma en cuenta el tamaño de la imagen mas sus margines
    document.querySelector(".project-container").style.left = `${newValue}px`;
  }
  
  /** Esta funcion se llama cuando la persona hace click en la fecha izquierda del carousel para navegar a la izquierda */
  function clickLeft() {
    const currentLeft = parseInt(
      getComputedStyle(document.querySelector(".project-container")).left,
      10
    );
    if (currentLeft === 0) { //si el valor de izquiera es 0, retornar para no seguir movierno el contenido
      return;
    }
    let newValue = currentLeft + 270;
    document.querySelector(".project-container").style.left = `${newValue}px`;
  }
  
  /** Esta funcion se llama cuando la persona hace click en el boton de enviar del formulario de contacto */
  function showNotification() {
    document.querySelector(".notification").style.display = "flex";
    setTimeout(function() {
      document.querySelector(".notification").style.display = "none";
    }, 3000);
  }
  
  